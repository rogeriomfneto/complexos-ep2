import jwt from 'jsonwebtoken';

const decodeToken = (token) => {
  return jwt.decode(token);
}

export default {
  async createUser({ commit, state }) {
    const payload = {
      email: state.email,
      username: state.username,
      password: state.password,
    };
    const response =  await this.$axios.$post('/register', payload);
    const token = decodeToken(response.accessToken);
    commit('setToken', response.accessToken);
    return token;
  },
  async getUserToken({ commit, state }) {
    const payload = {
      email: state.email,
      password: state.password,
    }
    const response =  await this.$axios.$post('/login', payload);
    const token = decodeToken(response.accessToken);
    commit('setToken', response.accessToken);
    return token;
  }
}
