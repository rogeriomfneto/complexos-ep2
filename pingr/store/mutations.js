export default {
  setEmail(state, email) {
    state.email = email;
  },
  setUsername(state, username) {
    state.username = username;
  },
  setPassword(state, password) {
    state.password = password;
  },
  setToken(state, token) {
    state.token = token;
    window.localStorage.setItem('token', token);
  },
  clearAll(state) {
    state.email = "";
    state.username = "";
    state.password = "";
  }
}
